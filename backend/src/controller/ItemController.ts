import { request, Request, Response } from 'express'
import { getRepository } from 'typeorm'
import * as Yup from 'yup'

import Item from '../models/Item'

export default {
  async index(req: Request, res: Response) {
    const repo = getRepository(Item)
    const items = await repo.find({
      relations: ['images']
    })
    return res.status(200).json(items)
  },

  async show(req: Request, res: Response) {
    const { id } = req.params
    const repo = getRepository(Item)
    const item = await repo.findOneOrFail(id, {
      relations: ['images']
    })
    return res.status(200).json(item)
  },

  async create(req: Request, res: Response) {
    const {
      title,
      description,
      value,
      category
    } = req.body
    const repo = getRepository(Item)
    const reqImages = req.files as Express.Multer.File[]
    const images = reqImages.map(image => {
      return { path: image.filename }
    })
    const data = {
      title,
      description,
      value,
      category,
      images
    }
    const schema = Yup.object().shape({
      title: Yup.string().required(),
      description: Yup.string().required().max(300),
      value: Yup.number().required(),
      category: Yup.number().required(),
      images: Yup.array(
        Yup.object().shape({
          path: Yup.string().required()
        })
      )
    })
    await schema.validate(data, {
      abortEarly: false
    })
    const item = repo.create(data)
    await repo.save(item)
    return res.status(201).json(item)
  }
}