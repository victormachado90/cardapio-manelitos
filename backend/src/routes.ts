import { Router } from 'express'
import multer from 'multer'

import uploadConfig from './config/upload'
import CategoryController from './controller/CategoryController'
import ItemController from './controller/ItemController'

const routes = Router()
const upload = multer(uploadConfig)

routes.get('/', (res, resp) => {
  return resp.status(200).json({
    message: 'Server up'
  })
})

// Category routes
routes.route('/category')
  .get(CategoryController.index)
  .post(CategoryController.create)
routes.route('/category/:id')
  .get(CategoryController.show)
  .put(CategoryController.update)
  .delete(CategoryController.delete)

// Item routes
routes.route('/item')
  .get(ItemController.index)
  .post(upload.array('images'), ItemController.create)

export default routes