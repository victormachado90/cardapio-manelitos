import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class createCategoryItemImages1604123709895 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.createTable(new Table({
        name: 'category_item_images',
        columns: [
          {
            name: 'id',
            type: 'integer',
            unsigned: true,
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'path',
            type: 'varchar'
          },
          {
            name: 'category_item_id',
            type: 'integer'
          }
        ],
        foreignKeys: [
          {
            name: 'ItemImageCategory',
            columnNames: ['category_item_id'],
            referencedTableName: 'category_items',
            referencedColumnNames: ['id'],
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE'
          }
        ]
      }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.dropTable('category_item_images')
    }

}
