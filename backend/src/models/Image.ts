import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm'
import Item from './Item'

@Entity('category_item_images')
export default class Image {
  @PrimaryGeneratedColumn('increment')
  id: number

  @Column()
  path: string
 
  @ManyToOne(() => Item, item => item.images)
  @JoinColumn({ name: 'category_item_id' })
  item: Item
}