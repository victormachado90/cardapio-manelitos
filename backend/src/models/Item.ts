import { Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinColumn, ManyToOne } from 'typeorm'
import Category from './Category'
import Image from './Image'

@Entity('category_items')
export default class Item {
  @PrimaryGeneratedColumn('increment')
  id: number

  @Column()
  title: string

  @Column()
  description: string

  @Column()
  value: number

  @ManyToOne(() => Category, category => category.items)
  @JoinColumn({ name: 'category_id' })
  category: Category

  @OneToMany(() => Image, image => image.item, {
    cascade: ['insert', 'update']
  })
  @JoinColumn({ name: 'category_item_id' })
  images: Image[]
}